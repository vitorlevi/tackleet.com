var express = require('express');
var cookieParser = require('cookie-parser');

var session = require('express-session');
var router = express.Router();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var favicon = require('serve-favicon');
var logger = require('morgan');

var http = require('http');
var path = require('path');
var fs = require('fs');

var passport = require('passport');
var dbConnection = require('./config/db-connection.js');
var Strategies = require('./config/passport-strategies.js');



// Start express application
var app = express();
var done=false;

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(methodOverride('X-HTTP-Method-Override'));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));                   // log every request to the console
app.use(bodyParser.urlencoded({ extended: true,limit: '50mb' }));    // parse application/x-www-form-urlencoded
app.use(cookieParser());
app.use(methodOverride());

// app.use(session({ secret: 'keyboard cat' }));
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'viewer')));

app.use('/',require("./routes/main"));
app.use('/api',require("./routes/user"));
app.use('/api',require("./routes/project"));
app.use('/api',require("./routes/issue"));
app.use('/auth',require("./routes/auth"));

// development only
var env = process.env.NODE_ENV || 'development';
if ('development' == env) {
   // configure stuff here
}




/* HTTP Services*/
http.createServer(app).listen(app.get('port'), function (){
	console.log('Express server listening on port ' + app.get('port'));
});
