/*
  ___                  __  __         _     _ 
 |_ _|_______  _ ___  |  \/  |___  __| |___| |
  | |(_-<_-< || / -_) | |\/| / _ \/ _` / -_) |
 |___/__/__/\_,_\___| |_|  |_\___/\__,_\___|_|

File: models/issue.js                                              
*/

var mongoose = require('mongoose')
  , Schema = mongoose.Schema

// create a user model
var issueSchema = Schema({
  number     : Number,
	title: String,
	name: String,
	org: String,
	repo: String,
	created_at: { type: Date, default: Date.now },
  _users : [{ type: Schema.Types.ObjectId, ref: 'User' }],
  _projects : [{ type: Schema.Types.ObjectId, ref: 'Project' }]
});
var Issue  = mongoose.model('Issue', issueSchema);

module.exports = Issue;


