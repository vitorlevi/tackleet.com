/*
  _   _               __  __         _     _ 
 | | | |___ ___ _ _  |  \/  |___  __| |___| |
 | |_| (_-</ -_) '_| | |\/| / _ \/ _` / -_) |
  \___//__/\___|_|   |_|  |_\___/\__,_\___|_|

 File: models/user.js

*/

var mongoose = require('mongoose')
  , Schema = mongoose.Schema

// create a user model
var userSchema = Schema({
	githubId: String,
	username: String,
	name: String,
	email: String,
	avatar: String,
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now },
	active: { type: Boolean, default: true },
  _issues : [{ type: Schema.ObjectId, ref: 'Issue' }],
  _projects : [{ type: Schema.ObjectId, ref: 'Project' }]
});
var User  = mongoose.model('User', userSchema);

module.exports = User;
