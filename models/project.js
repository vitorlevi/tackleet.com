/*
	___          _        _     __  __         _     _
 | _ \_ _ ___ (_)___ __| |_  |  \/  |___  __| |___| |
 |  _/ '_/ _ \| / -_) _|  _| | |\/| / _ \/ _` / -_) |
 |_| |_| \___// \___\__|\__| |_|  |_\___/\__,_\___|_|
						|__/

File: models/project.js

*/
var mongoose = require('mongoose')
	, Schema = mongoose.Schema


// create a user model
var projectSchema = Schema(
{
	number     : Number,
	tilte: String,
	name: String,
	org: String,
	repo: String,
	published: Boolean,
	created_at: { type: Date, default: Date.now },
	_users : [{ type: Schema.Types.ObjectId, ref: 'User' }]
});
var Project  = mongoose.model('Project', projectSchema);

module.exports = Project;
