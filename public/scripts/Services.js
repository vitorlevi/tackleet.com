/*
   ___           __ _        __  __         _      _
  / __|___ _ _  / _(_)__ _  |  \/  |___  __| |_  _| |___
 | (__/ _ \ ' \|  _| / _` | | |\/| / _ \/ _` | || | / -_)
  \___\___/_||_|_| |_\__, | |_|  |_\___/\__,_|\_,_|_\___|
                     |___/

Path: public/scripts/Config.js

*/

'use strict';

var app = angular.module('openSolver.services', ['openSolver.factory']);


app.service('AuthService', function($q, $http, $location)
{
  return {
    'checkLogin': function(user)
    {
      var defer = $q.defer();
    	if(user === undefined){
        // location.url('/login');
        $('.ui.basic.modal.signin').modal('show');
    	 	return false;
    	 }
    	 else
    	 {
    	 	return true;
    	 }
    }
  }
});

app.service('TackleService', function($q, $http, $location, IssueFactory, AuthService)
{
  return {
    'save': function(issue, user)
    {
      var defer = $q.defer();
      if(AuthService.checkLogin(user) === true){
        var data = {
    			number:issue.number,
    			title:issue.title,
          org:issue.org,
          name:issue.name,
    			_users: user._id,
    			_projects: issue._project._id,
    		};
        IssueFactory.save(data).then(function(res)
        {
  			  defer.resolve(res);
  			}, function(err) {
    			defer.reject(err);
    		});
      }
        return defer.promise;
    }
  }
});

app.service('ProjectService', function($q, $http, $location, ProjectFactory, AuthService)
{
  return {
    'add': function(project,user)
    {
      var defer = $q.defer();
      if(AuthService.checkLogin(user) === true){
        var data = {
  				number: project.id,
  				name: project.name,
  				org: project.owner.login,
          published: true,
  				_users : user._id
  			}
        ProjectFactory.save(data).then(function(res)
        {
  			  defer.resolve(res);
  			}, function(err) {
    			defer.reject(err);
    		});
      }
        return defer.promise;
    }
  }
});

app.service('SearchService', function($q, $http, $location, ProjectFactory) {
  return {
    'github': function(query) {
      var defer = $q.defer();
      ProjectFactory.getAll().then(function(projects)
      {
        var repos = [];
        for(var i in projects){
          repos.push('repo:'+projects[i].org+'/'+projects[i].name+'+');
        }
        // console.log(repos.join(""));
        $http.get('https://api.github.com/search/issues?q='+query+'+'+repos.join("")+'state:open').success(function(resp){
          defer.resolve(resp);
        }).error( function(err) {
          defer.reject(err);
        });
      });
      return defer.promise;
    }
  }
});
