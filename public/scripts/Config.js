/*
   ___           __ _        __  __         _      _
  / __|___ _ _  / _(_)__ _  |  \/  |___  __| |_  _| |___
 | (__/ _ \ ' \|  _| / _` | | |\/| / _ \/ _` | || | / -_)
  \___\___/_||_|_| |_\__, | |_|  |_\___/\__,_|\_,_|_\___|
                     |___/

Path: public/scripts/Config.js

*/

'use strict';

var app = angular.module('openSolver.config', []);


app.config(function($routeProvider, $locationProvider, $httpProvider)
{
    /*
    CHECK IF THE USER IS CONNECTED
    */
    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope)
    {
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/auth/loggedin').success(function(user){

        $rootScope.connectedUser = user;
        $rootScope.checkLogin = true;
        // Authenticated
        if (user !== '0'){
          $rootScope.connectedUser = user;
          deferred.resolve();
        }
        else
        {
          $rootScope.checkLogin = false;
          $('.ui.basic.modal.signin').modal('show');
          deferred.reject();
        }
      });

      return deferred.promise;
  };


  /*
  ADD AN INTERCEPTOR FOR AJAX ERROS
  */
  $httpProvider.interceptors.push(function($q, $location){
    return {
      response: function(response)
      {
        // Retrun response on success
        return response;
      },
      responseError: function(response){
        if (response.status === 401)
          $location.url('/#/login');
        return $q.reject(response);
      }
    };
  });

  /*
  DEFINE ALL ROUTES
  */
  $routeProvider
  .when('/',
  {
    templateUrl: 'views/home.html',
    controller: 'MainCtrl'
  })

  /*
  _                 _
 | |               (_)
 | |     ___   __ _ _ _ __
 | |    / _ \ / _` | | '_ \
 | |___| (_) | (_| | | | | |
 |______\___/ \__, |_|_| |_|
               __/ |
              |___/
  */
  .when('/login',
  {
      // templateUrl: 'views/Auth/login.html',
      controller: 'LoginCtrl',
      resolve: {
          app: function () {
            $('.ui.basic.modal.signin').modal('show');
          }
        }
  })
   .when('/login/redirect/',
   {
      templateUrl: 'views/Auth/redirect.html',
      controller: 'LoginRedirectCtrl',
      resolve: {
      loggedin: checkLoggedin
    }
  })
  .when('/register',
  {
      templateUrl: 'views/Auth/register.html',
      controller: 'RegisterCtrl'
  })

  /*
  _____           _           _
 |  __ \         (_)         | |
 | |__) | __ ___  _  ___  ___| |_
 |  ___/ '__/ _ \| |/ _ \/ __| __|
 | |   | | | (_) | |  __/ (__| |_
 |_|   |_|  \___/| |\___|\___|\__|
                _/ |
               |__/
  */
  .when('/project',
  {
    templateUrl: 'views/Project/list.html',
    controller: 'ProjectsCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/project/search',
  {
    templateUrl: 'views/Project/search.html',
    controller: 'ProjectsSearchCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/project/:id',
  {
    templateUrl: 'views/Project/detail.html',
    controller: 'ProjectCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/project/create',
  {
    templateUrl: 'views/Project/create.html',
    controller: 'ProjectCtrl',
    resolve: {
      loggedin: checkLoggedin,

    }
  })
  .when('/project/edit',
  {
    templateUrl: 'views/Project/edit.html',
    controller: 'ProjectCtrl',
    resolve: {
      loggedin: checkLoggedin
    }
  })


  /*
  _    _
 | |  | |
 | |  | |___  ___ _ __
 | |  | / __|/ _ \ '__|
 | |__| \__ \  __/ |
  \____/|___/\___|_|

  */

  .when('/users',
  {
    templateUrl: 'views/list.html',
    controller: 'UsersCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/user/perfil',
  {
    templateUrl: 'views/User/perfil.html',
    controller: 'UserPerfilCtrl',
      resolve: {
      loggedin: checkLoggedin,
      app: function ( $rootScope) {
        $rootScope.v_intro=true;
      }
    }
  })
  .when('/user/:id',
  {
    templateUrl: 'views/User/detail.html',
    controller: 'UserDetailCtrl',
    resolve: {
      app: function ( $rootScope) {
        $rootScope.v_intro=true;
      }
    }
  })
  .when('/my/issues',
  {
    templateUrl: 'views/User/issues.html',
    controller: 'UserIssuesCtrl',
    resolve: {
      loggedin: checkLoggedin,
      app: function ( $rootScope) {
        $rootScope.v_intro=true;
      }
    }
  })
  .when('/my/projects',
  {
    templateUrl: 'views/User/projects.html',
    controller: 'UserProjectsCtrl',
      resolve: {
      loggedin: checkLoggedin,
      app: function ( $rootScope) {
        $rootScope.v_intro=true;
      }
    }
  })
  .when('/publish/project',
  {
    templateUrl: 'views/User/publish.html',
    controller: 'UserProjectsCtrl',
      resolve: {
      loggedin: checkLoggedin,
      app: function ( $rootScope) {
        $rootScope.v_intro=true;
      }
    }
  })
  // .when('/user/:id',
  // {
  //   templateUrl: 'views/User/detail.html',
  //   controller: 'UserCtrl',
  //   resolve: {
  //       app: function ( $rootScope) {
  //         $rootScope.v_intro=true;
  //       }
  //     }
  // })
  .when('/user/edit/:id',
  {
    templateUrl: 'views/User/edit.html',
    controller: 'UserCtrl',
      resolve: {
      loggedin: checkLoggedin
    }
  })



  /*
  _____
 |_   _|
   | |  ___ ___ _   _  ___
   | | / __/ __| | | |/ _ \
  _| |_\__ \__ \ |_| |  __/
 |_____|___/___/\__,_|\___|

  */
  .when('/issue',
  {
    templateUrl: 'views/Issue/list.html',
    controller: 'IssueCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/issue/:id',
  {
    templateUrl: 'views/Issue/detail.html',
    controller: 'IssueDetailCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })
  .when('/issue/:org/:name/:number',
  {
    templateUrl: 'views/Issue/detail.html',
    controller: 'IssueDetailCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })


  .when('/search/:query',
  {
    templateUrl: 'views/Search/list.html',
    controller: 'SearchCtrl',
    resolve: {
        app: function ( $rootScope) {
          $rootScope.v_intro=true;
        }
      }
  })

  /*
   ____  _   _                        _
  / __ \| | | |                      (_)
 | |  | | |_| |__   ___ _ ____      ___ ___  ___
 | |  | | __| '_ \ / _ \ '__\ \ /\ / / / __|/ _ \
 | |__| | |_| | | |  __/ |   \ V  V /| \__ \  __/
  \____/ \__|_| |_|\___|_|    \_/\_/ |_|___/\___|

  */
  // TODO: Create a 404 page to support this case
  .otherwise(
  {
    redirectTo: '/',
    caseInsensitiveMatch: true
  })
});
