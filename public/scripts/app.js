/*
    _             
   /_\  _ __ _ __ 
  / _ \| '_ \ '_ \
 /_/ \_\ .__/ .__/
       |_|  |_|   

Path: public/scripts/app.js
       
*/
'use strict';

var app = angular.module('openSolver', ['ngRoute','openSolver.controllers', 'openSolver.factory', 'openSolver.config', 'openSolver.services']);




