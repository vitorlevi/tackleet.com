/*
	 ___         _           _ _              __  __         _      _
	/ __|___ _ _| |_ _ _ ___| | |___ _ _ ___ |  \/  |___  __| |_  _| |___
 | (__/ _ \ ' \  _| '_/ _ \ | / -_) '_(_-< | |\/| / _ \/ _` | || | / -_)
	\___\___/_||_\__|_| \___/_|_\___|_| /__/ |_|  |_\___/\__,_|\_,_|_\___|

Path: public/scripts/Controllers.js
*/

'use strict';

var app = angular.module('openSolver.controllers', ['openSolver.factory', 'openSolver.services']);


/*
  __  __       _
 |  \/  |     (_)
 | \  / | __ _ _ _ __
 | |\/| |/ _` | | '_ \
 | |  | | (_| | | | | |
 |_|  |_|\__,_|_|_| |_|

*/
app.controller('MainCtrl', function($rootScope, $scope, $location, ProjectFactory, AuthFactory, IssueFactory, AuthService, SearchService, TackleService )
{
	$scope.search = function(query)
	{
		$location.url('/search/'+query);
	}


	$scope.gitLogin = function(argument)
	{
		AuthFactory.github($location.path()).then(function(response) {
				console.log(response);
		});
	}
	// Signup in via Github
	$scope.gitSignup = function(argument) {
		$('.ui.basic.modal.signup').modal('show');
	}


	// Gets all shared projects
	ProjectFactory.getAll().then(function(projects)
	{
		$scope.projects = projects;
		// console.log(projects);
		// From found projects brings all its issues from GitHub
		IssueFactory.getFromGithub(projects).then(function(issues){
			$scope.issues = issues;

		});
	});

	$scope.getDetails = function(data)
	{
			console.log(data);
	}

	// Tackle Issue button
	$scope.tackleIssue = function (key,issue)
	{
			TackleService.save(issue, $scope.connectedUser).then(function(result)
			{
				$('#'+key).transition('scale', 200, function() { $scope.issues.splice(key, 1); });

			}, function(error) {
			  	alert('Could not save this issue: ' + error);
			});
	}
});

app.controller('HomeCtrl', function($rootScope,$scope,ProjectFactory, IssueFactory )
{


});


/*
  _                 _
 | |               (_)
 | |     ___   __ _ _ _ __
 | |    / _ \ / _` | | '_ \
 | |___| (_) | (_| | | | | |
 |______\___/ \__, |_|_| |_|
               __/ |
              |___/
*/

app.controller('LoginCtrl', function($rootScope, $scope, $routeParams, $http)
{
	// console.log($rootScope.connectedUser);
	$('.ui.basic.modal.signin').modal('show');

});

/* LOGIN REDIRECT  */
app.controller('LoginRedirectCtrl', function($rootScope, $scope, $location)
{
	$location.url('/');
});


/*
  _____           _           _
 |  __ \         (_)         | |
 | |__) | __ ___  _  ___  ___| |_
 |  ___/ '__/ _ \| |/ _ \/ __| __|
 | |   | | | (_) | |  __/ (__| |_
 |_|   |_|  \___/| |\___|\___|\__|
                _/ |
               |__/

*/

/* ALL PROJECTS */
app.controller('ProjectsCtrl', function($rootScope,$scope, $http,$route, AuthService, ProjectFactory)
{

	// console.log($scope.connectedUser);
	ProjectFactory.getAll().then(function(res) {
		$scope.projects = res;
	});

	$scope.remove = function(project)
	{
		var data = {project_id: project._id, user_id:$scope.connectedUser._id};
		// console.log(data);
		if(AuthService.checkLogin($scope.connectedUser))
			ProjectFactory.remove(data).then(function(res)
			{
				console.log(res);
			});
	}
});

app.controller('ProjectsSearchCtrl', function($rootScope,$scope, $http,$route, ProjectFactory, UserFactory, AuthService)
{


	$scope.search = function ()
	{
		console.log($scope.projectSearch);
		$http.get("https://api.github.com/search/repositories?q="+$scope.projectSearch )
		.then(function(res)
		{
				$scope.projects = res.data;
				console.log(res.data);
		});
	}
	$scope.subscribe = function (project)
	{
		if(AuthService.checkLogin($scope.connectedUser))
		{
			var data = {
				number: project.id,
				name: project.name,
				org: project.owner.login,
				_users : $scope.connectedUser._id
			}
			ProjectFactory.create(data).then(function(res)
			{
				console.log("saved project");
			});
		}


		console.log(data);

	}
});

/* PROJECT DETAIL */
app.controller('ProjectCtrl', function($scope, $http,$routeParams,$location, ProjectFactory, IssueFactory, AuthService)
{
	ProjectFactory.get($routeParams.id).then(function(project)
	{
		$scope.project = project;
		console.log(project);
		IssueFactory.getAllFromGithub(project).then(function(issues)
		{
			$scope.issues = issues;
			console.log(issues);
		});
	});

	$scope.tackleIssue = function (issue)
	{
		if(AuthService.checkLogin($scope.connectedUser))
		{
			var data = {
				number:issue.number,
				title:issue.title,
				name: $scope.project.name,
				org: $scope.project.org,
				_users: $scope.connectedUser._id,
				_projects: $scope.project._id,
			};

			console.log(data);

			/* Creating Issue from given project */
			IssueFactory.create(data).then(function(response)
			{
				console.log(data);
			});
		}
	}
});

/*
  _____
 |_   _|
   | |  ___ ___ _   _  ___
   | | / __/ __| | | |/ _ \
  _| |_\__ \__ \ |_| |  __/
 |_____|___/___/\__,_|\___|

*/

app.controller('IssueCtrl', function($scope, $http,$routeParams, IssueFactory, ProjectFactory, AuthService)
{



	ProjectFactory.getAll().then(function(res)
	{
		IssueFactory.getFromGithub(res).then(function(res){
			$scope.issues = res;
		});
	});

	$scope.tackleIssue = function (issue)
	{
		if(AuthService.checkLogin($scope.connectedUser))
		{
			var data =
			{
				number:issue.number,
				title:issue.title,
				name: issue.project.name,
				org: issue.project.org,
				_users: $scope.connectedUser._id,
				_projects: issue.project._id,
			};

			console.log(data);

			/* Creating Issue from given project */
			IssueFactory.create(data).then(function(response)
			{
				console.log(data);
			});
		}
	}
});

/* ISSUE DETAIL */
app.controller('IssueDetailCtrl', function($scope, $http,$routeParams, IssueFactory)
{
	var data =
	{
		org:$routeParams.org,
		name:$routeParams.name,
		number: $routeParams.number
	};
	IssueFactory.getDetails(data).then(function(res)
	{
		console.log(res);
		$scope.issue = res;
	});
	// console.log($routeParams.org, $routeParams.name, $routeParams.number);
});


/*
  _    _
 | |  | |
 | |  | |___  ___ _ __
 | |  | / __|/ _ \ '__|
 | |__| \__ \  __/ |
  \____/|___/\___|_|

*/
app.controller('UsersCtrl', function($rootScope, $scope, $routeParams, UserFactory, IssueFactory)
{

	// if ($routeParams.id !== undefined)
	UserFactory.get($routeParams.id).then(function(response)
	{
		UserFactory.getDetailsGithub(response.username).then(function(response)
		{
			$scope.user = response;
			console.log(response);
		});
	});
});
app.controller('UserDetailCtrl', function($rootScope, $scope, $routeParams, UserFactory, IssueFactory)
{

	// if ($routeParams.id !== undefined)
	UserFactory.get($routeParams.id).then(function(response)
	{
		UserFactory.getDetailsGithub(response.username).then(function(response)
		{
			$scope.user = response;
			console.log(response);
		});
	});

	// Issue.getUserIssues($routeParams.id).then(function(response)
	// {
	// 	$scope.issues = response;
	// });

 $scope.deleteIssue = function (issue)
 {
		IssueFactory.delete(issue).then(function(response)
		{
			console.log("Got Issue", issue);
		});
	}

});

app.controller('UserPerfilCtrl', function($rootScope, $scope, $routeParams, UserFactory, IssueFactory, ProjectFactory)
{

	// if ($routeParams.id !== undefined)
	UserFactory.getDetailsGithub($rootScope.connectedUser.username).then(function(response)
	{
		$scope.user = response;
		console.log(response);
	});

	$scope.deleteIssue = function (issue)
  {
 		IssueFactory.delete(issue).then(function(response)
 		{
 			console.log("Got Issue", issue);
 		});
 	}
});

/* USER PROJECTS */
app.controller('UserProjectsCtrl', function($scope, $routeParams, UserFactory, ProjectFactory, ProjectService)
{
	UserFactory.get($scope.connectedUser._id).then(function(response)
	{
		$scope.projects = response._projects;
	});

	UserFactory.getGithubRepo($scope.connectedUser.username).then(function(response)
	{
		$scope.gitProjects = response;

		for(var i in $scope.projects)
		{
			for(var x in $scope.gitProjects)
			{
				if($scope.gitProjects[x].id === $scope.projects[i].number)
				{
					$scope.gitProjects.splice(x, 1);
				}
			}
		}
	});

	// ADD PROJECT
	$scope.addProject = function (key,project)
	{
		ProjectService.add(project, $scope.connectedUser).then(function(result)
		{
			$('#'+key).addClass('inactive');
		}, function(error) {
				alert('Could not save this issue: ' + error);
		});
	}
});

/* USER ISSUES */
app.controller('UserIssuesCtrl', function($scope, $routeParams, UserFactory,IssueFactory)
{

	UserFactory.get($scope.connectedUser._id).then(function(response)
	{
		$scope.issues = response._issues;
		console.log(response);
	});
});




app.controller('SearchCtrl', function($scope, $routeParams, SearchService)
{
	SearchService.github($routeParams.query).then(function(response)
	{
			console.log(response);
			$scope.issues = response;
			$scope.therm = $routeParams.query;
	});
});
