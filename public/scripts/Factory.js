/*
  ___        _                  __  __         _      _
 | __|_ _ __| |_ ___ _ _ _  _  |  \/  |___  __| |_  _| |___
 | _/ _` / _|  _/ _ \ '_| || | | |\/| / _ \/ _` | || | / -_)
 |_|\__,_\__|\__\___/_|  \_, | |_|  |_\___/\__,_|\_,_|_\___|
                         |__/
Path: public/scripts/Factory.js
*/


'use strict';

var app = angular.module('openSolver.factory', []);

var API_PATH = '/api';


app.factory('AuthFactory', function($q, $http,$location) {
  return {
    'github': function(location) {
      var defer = $q.defer();
      $http.get('/auth/github').success(function(resp){
        defer.resolve(resp);
        $location.url('/'+location);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
  }
});

/*
  _____           _           _
 |  __ \         (_)         | |
 | |__) | __ ___  _  ___  ___| |_
 |  ___/ '__/ _ \| |/ _ \/ __| __|
 | |   | | | (_) | |  __/ (__| |_
 |_|   |_|  \___/| |\___|\___|\__|
                _/ |
               |__/
*/
app.factory('ProjectFactory', function($q, $http) {
  return {
    'getAll': function() {
      var defer = $q.defer();
      $http.get(API_PATH + '/project/').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'get': function(id) {
      var defer = $q.defer();
      $http.get(API_PATH + '/project/' + id).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'save': function(data) {
      var defer = $q.defer();
      $http.post(API_PATH + '/project/create', data).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'remove': function(data) {
      console.log(data.project_id, data.user_id);
      var defer = $q.defer();
      defer.resolve(data);
      $http.delete(API_PATH + '/project/'+data.project_id+"?user="+data.user_id).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'search': function() {
      var defer = $q.defer();
      $http.get(API_PATH + '/project/search?').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
  }
});

/*
  _    _
 | |  | |
 | |  | |___  ___ _ __
 | |  | / __|/ _ \ '__|
 | |__| \__ \  __/ |
  \____/|___/\___|_|

  */
app.factory('UserFactory', function($q, $http) {
  return {
    'getAll': function() {
      var defer = $q.defer();
      $http.get(API_PATH + '/user/').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'get': function(id) {
      var defer = $q.defer();
      $http.get(API_PATH + '/user/'+id).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getDetailsGithub': function(username) {
      var defer = $q.defer();
      $http.get('https://api.github.com/users/'+username).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getGithubRepo': function(username) {
      var defer = $q.defer();
      $http.get('https://api.github.com/users/'+username+'/repos').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'create': function(data) {
      var defer = $q.defer();
      $http.post('/todo/addTodo', data).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'remove': function(todo) {
      var defer = $q.defer();
      $http.post('/todo/removeTodo', todo).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
  }
});



  /*
  _____
 |_   _|
   | |  ___ ___ _   _  ___
   | | / __/ __| | | |/ _ \
  _| |_\__ \__ \ |_| |  __/
 |_____|___/___/\__,_|\___|

  */
app.factory('IssueFactory', function($q, $http, $location) {
  return {
    'getAll': function() {
      var defer = $q.defer();
      $http.get(API_PATH + '/issue/').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getUserIssues': function(userId) {
      var defer = $q.defer();
      $http.get(API_PATH + '/issue/').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'get': function(id) {
      var defer = $q.defer();
      $http.get(API_PATH + '/issue/'+id).success(function(resp){
        defer.resolve(resp);

      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getDetails': function(data)
    {
      var defer = $q.defer();

      $http.get("https://api.github.com/repos/"+ data.org +"/"+ data.name +"/issues/"+data.number).success(function(resp)
      {
        defer.resolve(resp);
      }).error( function(err)
      {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getOneFromGithub': function(data)
    {
      var defer = $q.defer();

      $http.get(API_PATH + '/issue/'+data).success(function(resp){
        var project = resp._projects[0];
        $http.get("https://api.github.com/repos/"+ project.org +"/"+ project.name +"/issues/"+resp.number).success(function(resp){
          defer.resolve(resp);
        });
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getAllFromGithub': function(data) {
      var defer = $q.defer();
      $http.get("https://api.github.com/repos/"+ data.org +"/"+ data.name +"/issues").success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'getFromGithub': function(data) {

      var defer = $q.defer();
      var gitIssues = [];
      var project = [];
      var cart2 = [];
      
      angular.forEach(data, function(item,key) {
        var call = $http.get("https://api.github.com/repos/"+ item.org +"/"+ item.name +"/issues");
        gitIssues = call;
        gitIssues.project = {_id: item._id, org:item.org, name:item.name};
        cart2.push(gitIssues);
        // project.push({_id: item._id, org:item.org, name:item.name});

      });
      $q.all(cart2).then(function(results)
      {
        var issues = {};
        var cart = [];
        var preview = {};

        for(var i in cart2)
        {
          results[i].project = cart2[i].project;

        }
        for(var x in results)
        {
          for(var i in results[x].data)
          {
            preview =results[x].data[i];
            preview._project = results[x].project;
            cart.push(preview);
          }
        };
        defer.resolve(cart)
      },
      function(errors) {
        defer.reject(errors);
      },
      function(updates){
        defer.update(updates);
      });

      return defer.promise;
    },
    'featured': function() {
      var defer = $q.defer();
      var gitIssues = [];
      $http.get(API_PATH + '/project').success(function(resp){
        // defer.resolve(resp);

        angular.forEach(resp ,function(item,key) {
          console.log(item, key);
          $http.get("https://api.github.com/repos/"+ item.org +"/"+ item.name +"/issues").success(function(resp){
            defer.resolve(resp)
          }).error( function(err) {
            defer.reject(err);
          });
        });
        console.log(defer);

      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'save': function(data) {
      var defer = $q.defer();
      $http.post(API_PATH + '/issue/save', data).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'remove': function(id) {
      var defer = $q.defer();
      $http.delete(API_PATH + '/issue/remove/' + id).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
  }
});
