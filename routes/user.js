module.exports = (function() {
	var express = require('express');
	var router = new express.Router();
	var User = require('../models/user.js');



	/*
	   _____                _
	  / ____|              | |
	 | |     _ __ ___  __ _| |_ ___
	 | |    | '__/ _ \/ _` | __/ _ \
	 | |____| | |  __/ (_| | ||  __/
	  \_____|_|  \___|\__,_|\__\___|

	*/


	/*
	Method: GET
	Path: /api/project/create
	Description: Create new user via URL query
	Return: project
	*/
	router.get('/user/create', function (req, res, next)
	{
		res.send(req.query);
		var user = new User(req.query);
		user.save(function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("saving user ...");
				done(null, user);
			};
		});
	});



	/*
	Method: POST
	Path: /api/user/create
	Description: Create new user
	Return: project
	*/
	router.post('/user/create', function (req, res, next)
	{
		var user = new User(req.body);
		user.save(function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("saving user ...");
				res.send(user);
				done(null, user);
			};
		});
	});


	/*
	  _____  ______          _____
	 |  __ \|  ____|   /\   |  __ \
	 | |__) | |__     /  \  | |  | |
	 |  _  /|  __|   / /\ \ | |  | |
	 | | \ \| |____ / ____ \| |__| |
	 |_|  \_\______/_/    \_\_____/

	*/

	/*
	Method: GET
	Path: /api/project
	Description: Return all projects
	Return: projects
	*/
	router.get('/user', function (req, res, next)
	{
		User.find({})
		.populate('_projects', 'number name org')
		.populate('_issues', 'number title')
		// .select('username')
		.exec(function (err, users) {
			if (err) return handleError(err);
				res.send(users)
		});
	});

	/* GET Gets all user */
	router.get('/user/:id', function (req, res, next)
	{
		User.findOne(req.params.id)
		.populate('_projects', 'number name org')
		.populate('_issues', 'number name org title')
		.exec(function (err, users) {
			if (err) return handleError(err);
				res.send(users)
		});
	});

	/* GET Query Search */
	router.get('/user/search', function (req, res, next)
	{
		var q = req.query;
		console.log(req.query);
		User.find()
		.where('active').equals(q.active)
		.limit(q.limit)
		.sort(req.query* req.order)
		// .select('githubId username')
		.exec(function (err, users) {
			if (err) console.log(err);
			res.send(users);
		})
	});



	/*
	  _    _           _       _
	 | |  | |         | |     | |
	 | |  | |_ __   __| | __ _| |_ ___
	 | |  | | '_ \ / _` |/ _` | __/ _ \
	 | |__| | |_) | (_| | (_| | ||  __/
	  \____/| .__/ \__,_|\__,_|\__\___|
	        | |
	        |_|
	*/


	/*
	  _____       _      _
	 |  __ \     | |    | |
	 | |  | | ___| | ___| |_ ___
	 | |  | |/ _ \ |/ _ \ __/ _ \
	 | |__| |  __/ |  __/ ||  __/
	 |_____/ \___|_|\___|\__\___|


	*/








	return router;
})();
