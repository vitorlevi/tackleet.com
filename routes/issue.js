module.exports = (function() 
{
	var express = require('express');
	var router = new express.Router();
	var Project = require('../models/project.js');
	var User = require('../models/user.js');
	var Issue = require('../models/issue.js');


	/*
	   _____                _       
	  / ____|              | |      
	 | |     _ __ ___  __ _| |_ ___ 
	 | |    | '__/ _ \/ _` | __/ _ \
	 | |____| | |  __/ (_| | ||  __/
	  \_____|_|  \___|\__,_|\__\___|
	                                
	*/

	/*	
	Method: POST
	Path: /api/issue/save
	Description: Save new issue
	Return: issue
	*/
	router.post('/issue/save', function (req, res, next) 
	{
		console.log(req.body._users);
		var issue = new Issue(req.body);
		issue.save(function (err) 
		{
			console.log("project ID ...", issue,req.body._users );
			User.findByIdAndUpdate(
			 	req.body._users, 
			 	{$push: { _issues: {_id:issue._id} }},
			 	{safe: true, upsert: true},
			 	function (err, user) {
				  if (err) return console.log(err);
				  console.log("User updated", user);
				  res.send(issue);
			});
		});
	});

	/*	
	Method: GET
	Path: /api/issue/save
	Description: Create new issue via URL query
	Return: issue
	*/
	router.get('/issue/save', function (req, res, next)
	{
		res.send(req.query);
		var issue = new Issue(req.query);
		issue.save(function(err) {
			if(err) { 
				 console.log(err); 
			} else {
				 console.log("saving issue ...");
				 next(null, issue);
			};
		});
	});


	/*
	  _____  ______          _____  
	 |  __ \|  ____|   /\   |  __ \ 
	 | |__) | |__     /  \  | |  | |
	 |  _  /|  __|   / /\ \ | |  | |
	 | | \ \| |____ / ____ \| |__| |
	 |_|  \_\______/_/    \_\_____/ 
	                                
	*/

	/*	
	Method: GET
	Path: /api/issue
	Description: Return all issue
	Return: populated issue
	*/
	router.get('/issue', function (req, res, next) 
	{
		Issue.find({})
		.populate('_users', 'username name avatar')
		.populate('_projects', 'number name org')
		.exec(function (err, issue) {
			if (err) return handleError(err);
			res.send(issue)
			// prints "The creator is Aaron"
		});
	});

	/*	
	Method: GET
	Path: /api/project
	Description: Return all projects
	Return: projects
	*/
	router.get('/issue/:id', function (req, res, next) 
	{
		// res.send(req.params.id);
		Issue.findOne({_id: req.params.id})
		.populate('_users', 'username name avatar')
		.populate('_projects', 'number name org')
		.exec(function (err, projects) {
			if (err) console.log(err);
			res.send(projects)
			// prints "The creator is Aaron"
		});
	});

	/*	
	Method: GET
	Path: /api/issue/search
	Description: Sesarch for a issue
	Return: issue
	*/
	router.get('/issue/search', function (req, res, next)
	{
		var q = req.query;
		console.log(req.query);

		Issue.find({})
		 .where('active').equals(q.active)
		 .limit(q.limit)
		 .sort(req.query* req.order)
		 .populate('_users')
		 .populate('_projects')
		 // .select('githubId username')
		 .exec(function (err, issue) {
				if (err) console.log(err);
				 res.send(issue);
			})
	});


	/*
	  _    _           _       _       
	 | |  | |         | |     | |      
	 | |  | |_ __   __| | __ _| |_ ___ 
	 | |  | | '_ \ / _` |/ _` | __/ _ \
	 | |__| | |_) | (_| | (_| | ||  __/
	  \____/| .__/ \__,_|\__,_|\__\___|
	        | |                        
	        |_|                        
	*/




	/*
	  _____       _      _       
	 |  __ \     | |    | |      
	 | |  | | ___| | ___| |_ ___ 
	 | |  | |/ _ \ |/ _ \ __/ _ \
	 | |__| |  __/ |  __/ ||  __/
	 |_____/ \___|_|\___|\__\___|                             
	*/
	/*	
	Method: GET
	Path: /api/issue/remove/:id
	Description: Delete a project from system
	Return: project
	*/
	router.get('/project/remove/:id', function (req, res, next) 
	{
		Issue.findOneAndRemove({_id: req.params.id})
			.exec(function (err, issue) {
				if (err) console.log(err);
				res.send(200);
			});
	});


	return router;
})();