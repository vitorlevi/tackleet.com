module.exports = (function() {
    var express = require('express');
    var router = new express.Router();
    var passport = require('passport');
    var Strategies = require('../config/passport-strategies.js');

    var User = require('../models/user.js');


    // route to test if the user is logged in or not
    router.get('/loggedin', function (req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    });


    /* Log out */
    router.get('/logout', function (req, res){
        req.logOut('teste 123');
        res.redirect('/#/');

    });

    router.get('/github',
      passport.authenticate('github', { scope: [ 'user:email' ] }));

    router.get('/github/callback',
      passport.authenticate('github', { failureRedirect: '/login' }),
      function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/#/login/redirect');
      });


    // Facebook Authentication
    router.get('/facebook',
        passport.authenticate('facebook'),
        function(req, res){
        });
    router.get('/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: '/' }),
        function(req, res) {
            res.redirect('/');
            console.log(res);
        }
    );

    return router;
})();
