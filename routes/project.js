module.exports = (function() {
	var express = require('express');
	var router = new express.Router();
	var Project = require('../models/project.js');
	var User = require('../models/user.js');


/*
	   _____                _       
	  / ____|              | |      
	 | |     _ __ ___  __ _| |_ ___ 
	 | |    | '__/ _ \/ _` | __/ _ \
	 | |____| | |  __/ (_| | ||  __/
	  \_____|_|  \___|\__,_|\__\___|
	                                
	*/

	/*	
	Method: POST
	Path: /api/project/create
	Description: Create new project
	Return: project
	*/
	router.post('/project/create', function ( req, res, next) 
	{
		var project = new Project(req.body);
		project.save(function (err) 
		{
			var projectID = project._id;
			console.log("project ID ...", project,req.body._users );
			User.findByIdAndUpdate(
			 	req.body._users, 
			 	{$push: { _projects: {_id:projectID} }},
			 	{safe: true, upsert: true},
			 	function (err, user) {
				  if (err) return console.log(err);
				  console.log("User updated", user);
				  res.send(project);
			});
		});
	});

	/*	
	Method: GET
	Path: /api/project/create
	Description: Create new project via URL query
	Return: project
	*/
	router.get('/project/create', function (req, res, next)
	{
		res.send(req.query);
		var project = new Project(req.query);
		project.save()
		.exec(function (err, project) {
				if (err) return handleError(err);
				res.send(projects)
		});
	});


	/*
	  _____  ______          _____  
	 |  __ \|  ____|   /\   |  __ \ 
	 | |__) | |__     /  \  | |  | |
	 |  _  /|  __|   / /\ \ | |  | |
	 | | \ \| |____ / ____ \| |__| |
	 |_|  \_\______/_/    \_\_____/ 
	                                
	*/

	/*	
	Method: GET
	Path: /api/project
	Description: Return all projects
	Return: projects
	*/
	router.get('/project', function (req, res, next) 
	{
		Project.find({})
		.populate('_users', 'username name avatar')
		.exec(function (err, projects) {
			if (err) return handleError(err);
			res.send(projects)
			// prints "The creator is Aaron"
		});
	});

	/*	
	Method: GET
	Path: /api/project
	Description: Return all projects
	Return: projects
	*/
	router.get('/project/:id', function (req, res, next) 
	{
		// res.send(req.params.id);
		Project.findOne({_id: req.params.id})
		.populate('_users')
		.exec(function (err, projects) {
			if (err) console.log(err);
			res.send(projects)
			// prints "The creator is Aaron"
		});
	});

	/*	
	Method: GET
	Path: /api/project/search
	Description: Sesarch for a project
	Return: project
	*/
	router.get('/project/search', function (req, res, next)
	{
		var q = req.query;
		console.log(req.query);

		Project.find({})
		 .where('active').equals(q.active)
		 .limit(q.limit)
		 .sort(req.query* req.order)
		 .populate('_users')
		 // .select('githubId username')
		 .exec(function (err, project) {
				if (err) console.log(err);
				 res.send(project);
			})
	});


	/*
	  _    _           _       _       
	 | |  | |         | |     | |      
	 | |  | |_ __   __| | __ _| |_ ___ 
	 | |  | | '_ \ / _` |/ _` | __/ _ \
	 | |__| | |_) | (_| | (_| | ||  __/
	  \____/| .__/ \__,_|\__,_|\__\___|
	        | |                        
	        |_|                        
	*/




	/*
	  _____       _      _       
	 |  __ \     | |    | |      
	 | |  | | ___| | ___| |_ ___ 
	 | |  | |/ _ \ |/ _ \ __/ _ \
	 | |__| |  __/ |  __/ ||  __/
	 |_____/ \___|_|\___|\__\___|                             
	*/

	/*	
	Method: GET
	Path: /api/project/delete/:id
	Description: Delete a project from system
	Return: project
	*/
	router.get('/project/delete/:id', function (req, res, next) 
	{
		Project.findOneAndRemove({_id: req.params.id})
			.exec(function (err, project) {
				if (err) console.log(err);
				res.send(project);
			});
	});


	/*	
	Method: GET
	Path: /api/project/remove/:id
	Description: Remove a project from a user:id
	Return: User
	*/

	// TODO: Just remove a project from the user that has did it
	router.delete('/project/:id?', function (req, res, next) 
	{
		console.log(req.query.user,req.params.id);
		var q = req.query.user;

		Project.findOneAndRemove({_id: req.params.id})
			.exec(function (err, issue) {
				if (err) console.log(err);
				res.send(200);
			});
		// Project.findOne({_id: req.params.id})
		// 	.exec(function (err, project) {
		// 		if (err) console.log(err);
		// 		res.send(project._users);

		// 		for(i in project._users)
		// 			console.log(project._users[i]);
		// 			User.findByIdAndUpdate(
		// 				project._users[i], 
		// 			 	{$pull: { _projects: {_id:project._id} }},
		// 			 	{safe: true, upsert: true},
		// 			 	function (err, user) {
		// 				  if (err) return console.log(err);
		// 				  console.log("User updated", user);
		// 				  res.send(200);
		// 			});
		// 	});
	});



	return router;
	})();